# DSFR BLOCK

Allows you to create block types based on DSFR.

## Install with Drush

```shell
drush en dsfr_block -y
```

## Check fields for block_content

```
/admin/dsfr/fields/block_content
```

## Author

Code created by Jérôme Bouquet (https://www.drupal.org/u/netprogfr)
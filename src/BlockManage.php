<?php

namespace Drupal\dsfr_block;

use Drupal\block_content\Entity\BlockContentType;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

use Drupal\Core\Controller\ControllerBase;

class BlockManage extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function getBlockTypes() {

    $block_types = \Drupal::entityTypeManager()->getStorage('block_content_type')->loadMultiple();
    $type_names = [];
    foreach ($block_types as $block_type) {
      $type_names[] = $block_type->id();
    }

    return $type_names;
  }

  /**
   * {@inheritdoc}
   */
  public function getListBlockTypesDesired() {

    $blocks_type = \Drupal::service('dsfr_block.blockStorage')->typeBlocks();

    foreach ($blocks_type as $k => $v) {

      $list[$k] = $k; 
    }

    return $list;
  }

  /**
   * {@inheritdoc}
   */
  function createBlockTypes( $list ) {

    $blocks_type = \Drupal::service('dsfr_block.blockStorage')->typeBlocks();

    foreach( $list  as $key => $value ) {

      if( $value != 0 ) {

        $new_block_type = BlockContentType::create([
    
          'id' => $key,
          'label' => $blocks_type[$key]['label'],
          'description' => $blocks_type[$key]['desc'],
        ]);
        $new_block_type->save();

        $fields = $blocks_type[$key]['fields'];

        foreach( $fields as $field_name ) {

          $this->blockContentAddField($key, $field_name);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  function blockContentAddField (
    $block_type_id,
    $field_name,
    $form_display_type = 'text_textfield', // or 'text_textarea_with_summary'
    $view_display_type = 'text_default',
    $label_show = 'hidden'
    ) 
  {
    // Add or remove the body field, as needed.
    $field = FieldConfig::loadByName(
      'block_content', 
      $block_type_id, 
      $field_name
    );

    if ( empty($field) ) { 

      // FIELD STORAGE
      $storage = \Drupal::service('dsfr_core.fieldStorage')->fieldData();

      $field_storage = FieldStorageConfig::loadByName( 'block_content', $field_name );
      if (empty( $field_storage )) {

        $field_storage = \Drupal::service('dsfr_core.fieldManage')->createFieldsStorage( 
          $field_name, 
          $storage[$field_name], 
          'block_content' 
        ); 
      }

      // FIELD
      $field = \Drupal::service('dsfr_core.fieldManage')->createFields(
        $storage[$field_name], 
        $field_storage,
        $block_type_id // bundle
      );

      // DISPLAY
      if( isset($storage[$field_name]['display']) && $storage[$field_name]['display'] != NULL ) {
        
        $form_display_type = $storage[$field_name]['display'];
      }
      
      \Drupal::service('dsfr_core.fieldManage')->displayFields(
        'block_content',
        $block_type_id,
        $field_name,
        $form_display_type
      );

      // VIEW
      if( isset($storage[$field_name]['view']) && $storage[$field_name]['view'] != NULL ) {
        
        $view_display_type = $storage[$field_name]['view'];
      }

      \Drupal::service('dsfr_core.fieldManage')->viewFields(
        'block_content',
        $block_type_id,
        $field_name,
        $view_display_type,
        $label_show
      );

      return $field;
    }
  }
}
<?php
/**
 * @file
 * Contains \Drupal\dsfr_block\Form\CreatetypeblocksForm.
 */
namespace Drupal\dsfr_block\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;

class NoticeBlockForm extends FormBase {

  private $table = 'dsfr_block_notice';

  /**
   * @return string
   */
  public function getFormId()  {

    return 'dsfr_block_notice_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Init tools for database and timezone
    $tools = \Drupal::service( 'dsfr_core.tools' );
    // dev: $tools->dropTable( $this->table, false );

    // Variables initiation
    $message = $date_end = NULL;
    $close = 0;
    // Time config
    $year = date('Y');
    $date_start = $year .'-01-01 00:00:00';
    $timezone = $tools->timeZone();

    // Create notice table if this table doesn't exist
    if( $tools->checkTable( $this->table ) == false ) {

      $settings = \Drupal::service('dsfr_block.blockStorage')->fieldsBlockNotice();
      $tools->createTable( $this->table, $settings );
    
      $tools->insertTable(
        $this->table,
        [
          'message' => $message,
          'close' => $close,
          'date_start' => $date_start
        ]
      );    
    } 

    // Get notice table values
    $data = $tools->selectTable( $this->table, '*', "WHERE id='1'" ); 
    if( count($data) > 0 ) {

      $message = $data[0]->message;
      $close = $data[0]->close;
      $date_start = $data[0]->date_start;
      $date_end = $data[0]->date_end;
    }

    // ------------------------------------------------------------------------------------------------------------ //
    // Form
    $form['message'] = [

      //'#type' => 'text_format',
      //'#format' => 'restricted_html_dsfr',
      '#type' => 'textfield',

      '#title' => $this->t('Message'),
      '#default_value' => $message,
      '#required' => true
    ];

    $form['close'] = [
      '#type' => 'checkbox',
      '#default_value' => $close,
      '#title' => $this->t('Allow the user to close the banner.'),
    ];

    $form['date_start'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Publication start date'),
      '#default_value' => new DrupalDateTime( $date_start ),
      '#date_timezone' => $timezone,
      '#required' => true
    ];

    $form['date_end'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Publication end date'),
      '#description' => $this->t('With or without an end date, the maximum duration is one month. You can simply shorten this period. Repeat the operation after one month if necessary.'),
      '#date_timezone' => $timezone,
    ];
    if( $date_end != NULL ) {

      $form['date_end']['#default_value'] =  new DrupalDateTime( $date_end );
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    //$now = time();
    $start = $form_state->getValue('date_start')->getTimeStamp();
    if( $form_state->getValue('date_end') != NULL ) {

      $end = $form_state->getValue('date_end')->getTimeStamp();

      if( $end < $start ) {
  
        $form_state->setErrorByName('date_end', $this->t('End date cannot be less than start date.'));
      }
    }    
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
    // Init tools for database
    $tools = \Drupal::service( 'dsfr_core.tools' );
    $fields = [];

    // Get values
    $fields['message'] = $form_state->getValue('message');
    $fields['close'] = $form_state->getValue('close');

    $fields['date_start'] = $form_state->getValue('date_start')->format("Y-m-d H:i:s");
    // Other way
    // $date_start =  $form_state->getValue('date_start')->getTimeStamp();
    // $date = new DrupalDateTime('now');
    // $date->setTimezone(new \DateTimeZone( $tools->timeZone() ));
    // $date_start_formatted = $date->format('Y-m-d H:i:s');

    if( $form_state->getValue('date_end') != NULL ) {

      $fields['date_end'] = $form_state->getValue('date_end')->format("Y-m-d H:i:s");

    } else {

      $start = $form_state->getValue('date_start')->getTimeStamp();
      $end = $start + 864000; // + 10 days
      $fields['date_end'] = date( 'Y-m-d H:i:s', $end );
    }

    $tools->updateTable( $this->table, $fields, 'id', 1 );
  }
}